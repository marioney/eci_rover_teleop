#include <eci_rover_teleop/eci_rover_teleop.h>

EciRoverTeleop::EciRoverTeleop():
linear_x_(1),
linear_y_(0),
angular_(2),
linear_z_(3)
{
    current_vel = 0.1;

    //JOYSTICK PAD TYPE
    nh_.param<std::string>("pad_type",pad_type_,"ps3");
    //
    nh_.param("num_of_buttons", num_of_buttons_, DEFAULT_NUM_OF_BUTTONS);
    // MOTION CONF
    nh_.param("axis_linear_x", linear_x_, DEFAULT_AXIS_LINEAR_X);
    nh_.param("axis_linear_y", linear_y_, DEFAULT_AXIS_LINEAR_Y);
    nh_.param("axis_linear_z", linear_z_, DEFAULT_AXIS_LINEAR_Z);
    nh_.param("axis_angular", angular_, DEFAULT_AXIS_ANGULAR);
    nh_.param("scale_angular", a_scale_, DEFAULT_SCALE_ANGULAR);
    nh_.param("scale_linear", l_scale_, DEFAULT_SCALE_LINEAR);
    nh_.param("scale_linear_z", l_scale_z_, DEFAULT_SCALE_LINEAR_Z);
    nh_.param("cmd_topic_vel", cmd_topic_vel_, cmd_topic_vel_);
    nh_.param("button_dead_man", dead_man_button_, dead_man_button_);
    nh_.param("button_speed_up", speed_up_button_, speed_up_button_);  //4 Thrustmaster
    nh_.param("button_speed_down", speed_down_button_, speed_down_button_); //5 Thrustmaster
    nh_.param<std::string>("joystick_dead_zone", joystick_dead_zone_, "true");

      // DIGITAL OUTPUTS CONF
      nh_.param("cmd_service_io", cmd_service_io_, cmd_service_io_);
      nh_.param("button_output_1", button_output_1_, button_output_1_);
      nh_.param("button_output_2", button_output_2_, button_output_2_);
      nh_.param("output_1", output_1_, output_1_);
      nh_.param("output_2", output_2_, output_2_);

      ROS_INFO("SummitXLPad num_of_buttons_ = %d", num_of_buttons_);

      for(int i = 0; i < num_of_buttons_; i++){
          bRegisteredButtonEvent[i] = false;
          ROS_INFO("bREG %d", i);
          }

      for(int i = 0; i < 3; i++){
        bRegisteredDirectionalArrows[i] = false;
      }


      // Publish through the node handle Actuator Control
      vel_pub_ = nh_.advertise<mavros_msgs::ActuatorControl>(cmd_topic_vel_, 1);

      // Listen through the node handle sensor_msgs::Joy messages from joystick
      // (these are the references that we will sent to summit_xl_controller/command)
      pad_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &EciRoverTeleop::padCallback, this);

      bOutput1 = bOutput2 = false;

      // Topics freq control
      min_freq_command = min_freq_joy = 5.0;
      max_freq_command = max_freq_joy = 50.0;
      bEnable = false;	// Communication flag disabled by default
      last_command_ = true;
  }
void EciRoverTeleop::padCallback(const sensor_msgs::Joy::ConstPtr& joy)
{
    mavros_msgs::ActuatorControl vel;

    vel.controls.at(0) = 0; // ros roll
    vel.controls.at(1) = 0; // ros pitch
    vel.controls.at(2) = 0; // ros yaw
    vel.controls.at(3) = 0; // ros throttle
    vel.controls.at(4) = 0;
    vel.controls.at(5) = 0;
    vel.controls.at(6) = 0;
    vel.controls.at(7) = 0;

    bEnable = (joy->buttons[dead_man_button_] == 1);

    // Actions dependant on dead-man button
    if (joy->buttons[dead_man_button_] == 1) {
        //ROS_ERROR("SummitXLPad::padCallback: DEADMAN button %d", dead_man_button_);
        // Set the current velocity level
        if ( joy->buttons[speed_down_button_] == 1 ){

            if(!bRegisteredButtonEvent[speed_down_button_])
                if(current_vel > 0.1){
                    current_vel = current_vel - 0.1;
                    bRegisteredButtonEvent[speed_down_button_] = true;
                    ROS_INFO("Velocity: %f%%", current_vel*100.0);
                    char buf[50]="\0";
                    int percent = (int) (current_vel*100.0);
                    sprintf(buf," %d percent", percent);
                    // sc.say(buf);
                }
        }else{
            bRegisteredButtonEvent[speed_down_button_] = false;
         }

        if (joy->buttons[speed_up_button_] == 1){
            if(!bRegisteredButtonEvent[speed_up_button_])
                if(current_vel < 0.9){
                    current_vel = current_vel + 0.1;
                    bRegisteredButtonEvent[speed_up_button_] = true;
                    ROS_INFO("Velocity: %f%%", current_vel*100.0);
                    char buf[50]="\0";
                    int percent = (int) (current_vel*100.0);
                    sprintf(buf," %d percent", percent);
                    // sc.say(buf);
                }

        }else{
            bRegisteredButtonEvent[speed_up_button_] = false;
        }




        vel.controls.at(3) = current_vel*l_scale_*joy->axes[linear_x_];


        if(joystick_dead_zone_=="true")
        {
            // limit scissor movement or robot turning (they are in the same joystick)
            if(joy->axes[angular_] == 1.0 || joy->axes[angular_] == -1.0) // if robot turning
            {
                // Same angular velocity for the three axis
                vel.controls.at(2) = current_vel*(a_scale_*joy->axes[angular_]);

            }
            else if (joy->axes[linear_z_] == 1.0 || joy->axes[linear_z_] == -1.0) // if scissor moving
            {

                vel.controls.at(2)  = 0.0;
            }
            else
            {
                // Same angular velocity for the three axis
                vel.controls.at(2) = current_vel*(a_scale_*joy->axes[angular_]);

            }
        }
        else // no dead zone
        {

            vel.controls.at(2) = current_vel*(a_scale_*joy->axes[angular_]);

        }

    }
    else {
       vel.controls.at(2) = 0.0;
       vel.controls.at(3) = 0.0;
    }

     // Publish
    // Only publishes if it's enabled
    //if(bEnable){
        vel_pub_.publish(vel);

        last_command_ = true;
    //    }


    if(!bEnable && last_command_){
        vel.controls.at(2) = 0.0;
        vel.controls.at(3) = 0.0;
        vel_pub_.publish(vel);

        last_command_ = false;
        }
}





int main(int argc, char** argv)
{
    ros::init(argc, argv, "summit_xl_pad");
    EciRoverTeleop eci_rover_teleop;
    ros::spin();
}
