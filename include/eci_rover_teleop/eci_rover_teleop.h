#ifndef ECI_ROVER_TELEOP_H
#define ECI_ROVER_TELEOP_H

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <mavros_msgs/ActuatorControl.h>


#define DEFAULT_NUM_OF_BUTTONS		16
#define DEFAULT_AXIS_LINEAR_X		1
#define DEFAULT_AXIS_LINEAR_Y       0
#define DEFAULT_AXIS_ANGULAR		2
#define DEFAULT_AXIS_LINEAR_Z       3
#define DEFAULT_SCALE_LINEAR		1.0
#define DEFAULT_SCALE_ANGULAR		2.0
#define DEFAULT_SCALE_LINEAR_Z      1.0

class EciRoverTeleop
{
public:
    EciRoverTeleop();
private:
    void padCallback(const sensor_msgs::Joy::ConstPtr& joy);
    ros::NodeHandle nh_;

    int linear_x_, linear_y_, linear_z_, angular_;
    double l_scale_, a_scale_, l_scale_z_;
    //! It will publish into command velocity (for the robot) and the ptz_state (for the pantilt)
    ros::Publisher vel_pub_;
    //! It will be suscribed to the joystick
    ros::Subscriber pad_sub_;
    //! Name of the topic where it will be publishing the velocity
    std::string cmd_topic_vel_;
    //! Name of the service where it will be modifying the digital outputs
    std::string cmd_service_io_;
    double current_vel;
    //! Pad type
    std::string pad_type_;
    //! Number of the DEADMAN button
    int dead_man_button_;
    //! Number of the button for increase or decrease the speed max of the joystick
    int speed_up_button_, speed_down_button_;
    int button_output_1_, button_output_2_;
    int output_1_, output_2_;
    bool bOutput1, bOutput2;

    //! Number of buttons of the joystick
    int num_of_buttons_;
    //! Pointer to a vector for controlling the event when pushing the buttons
    bool bRegisteredButtonEvent[DEFAULT_NUM_OF_BUTTONS];
    //! Pointer to a vector for controlling the event when pushing directional arrows (UNDER AXES ON PX4!)
    bool bRegisteredDirectionalArrows[4];

        //! Diagnostics min freq
    double min_freq_command, min_freq_joy; //
    //! Diagnostics max freq
    double max_freq_command, max_freq_joy; //
    //! Flag to enable/disable the communication with the publishers topics
    bool bEnable;
    //! Flag to track the first reading without the deadman's button pressed.
    bool last_command_;
    //! Add a dead zone to the joystick that controls scissor and robot rotation (only useful for xWam)
    std::string joystick_dead_zone_;
};



#endif // ECI_ROVER_TELEOP_H
